# Hello, World!

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A simple dockerized web app

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```bash
yarn install
```

## Usage

```bash
yarn start
# OR
docker run ...
```

## Helm Chart

Before publishing an update to the Helm chart, first the respective versions of `charts/**/Chart.yaml` must be incremented. If a new application version is being used then the chart version should also be incremented accordingly.

```bash
# Package Helm Chart
yarn run helm:package

# Add package to git
git add -f charts/*.tgz

# Commit changes
# git commit -m '...'
```

The [chart index](https://noroff-accelerate.gitlab.io/cloud/projects/hello-world/index.yaml) is automatically updated when changes are made to `master`.

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Noroff Accelerate AS
