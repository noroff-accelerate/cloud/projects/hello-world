
/**
 * Dependencies
 * @ignore
 */
const express = require('express')
const morgan = require('morgan')

/**
 * Service
 * @ignore
 */
const app = express()
let count = 0

app.use(morgan('tiny'))
app.get('*', (req, res) => res.status(200).send(`<h1>Hello, Docker!</h1><p>Count: ${++count}</p>`))

/**
 * Listen
 * @ignore
 */
app.listen(process.env.PORT || 3000)
